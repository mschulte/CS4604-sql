drop table reserves;
drop table boats;
drop table sailors;
create table sailors (
  sid int primary key,
  sname varchar(32) not null,
  rating int not null,
  age real not null
);

insert into sailors (sid, sname, rating, age) values (28, 'yuppy', 9, 35.0);
insert into sailors (sid, sname, rating, age) values (31, 'lubber', 8, 55.5);
insert into sailors (sid, sname, rating, age) values (44, 'guppy', 5, 35.0);
insert into sailors (sid, sname, rating, age) values (58, 'rusty', 10, 35.0);

create table boats(
  bid int primary key,
  bname varchar(32) not null,
  color varchar(32) not null
);

insert into boats(bid, bname, color) values (101, 'Interlake', 'blue');
insert into boats(bid, bname, color) values (102, 'Pontoon', 'green');
insert into boats(bid, bname, color) values (103, 'Speed', 'red');

create table reserves (
  sid int not null,
  bid int not null,
  day date not null,
  primary key (sid,bid,day),
  foreign key (sid) references sailors,
  foreign key (bid) references boats
);

insert into reserves (sid, bid, day) values (28, 101, cast('10/10/96' as date));
insert into reserves (sid, bid, day) values (58, 103, cast('11/12/96' as date));

