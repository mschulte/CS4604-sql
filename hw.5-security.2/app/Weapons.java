import java.sql.*;

public class Weapons {
  static Connection db;

  public static void usage() {
    System.out.println("Usage: ./weapons <action> [args]\n" + 
      "Possible actions are: names, cost, setCost\n" +
      "Examples:\n" +
      "  To get the list of all Weapon Names use: ./weapons names\n" +
      "  To get the cost of a specific weapon, use: ./weapons cost \"Spoon\"\n" +
      "  To set the cost of a specific weapon, use: ./weapons setCost \"Spoon\" 5\n");
  }

  public static void getNames() {
    // We only allow users to manipulate cards of type 'WEAPON'
    String query = "select distinct name from cards where type = 'WEAPON' order by name";
    System.out.println("Weapon\n------");
    try {
      Statement stmt = db.createStatement();
      ResultSet rs = stmt.executeQuery(query);

      while (rs.next()) {
        System.out.println(rs.getString("NAME"));
      }
    } catch(Exception e) {
      throw new RuntimeException("You clearly did something wrong. My code does not have bugs!", e);
    }
  }

  public static void getCost(String cardName) {
    // Make sure that whatever name they ask for, we only show it if it is of type WEAPON
    String query = "select distinct name, type, cost from cards where type = 'WEAPON' and name = '" + cardName + "'";
    try {
      Statement stmt = db.createStatement();
      ResultSet rs = stmt.executeQuery(query);

      // We should only get one row, but this is the easiest way to get the results back
      while (rs.next()) {
        System.out.println("Name: " + rs.getString("NAME") + ", Type: " + 
          rs.getString("TYPE") + ", Cost: " + rs.getInt("COST"));
      }
    } catch(Exception e) {
      throw new RuntimeException("You clearly did something wrong. My code does not have bugs!", e);
    }
  }

  public static void setCost(String cardName, Integer newCost) {
    // Again, we can only update cards of type WEAPON!
    String query = "update cards set cost = " + newCost + " where type = 'WEAPON' and name = '" + cardName + "'";
    try {
      PreparedStatement pstmt = db.prepareStatement(query);
      pstmt.executeUpdate();
      // After updating, show the user the result
      getCost(cardName);
    } catch(Exception e) {
      throw new RuntimeException("You clearly did something wrong. My code does not have bugs!", e);
    }
  }

  public static void main(String[] args) {
    System.out.println("\n\n\n");
    System.out.println("The SUPER-COOL-WEAPON-COST Tool!\n\n");
    if (args.length == 0) {
      usage();
      System.exit(1);
    }
    db = ConnectionFactory.getConnection();

    // Call a function based on the action selected
    String action = args[0];
    switch (action) {
      case "names": getNames();
        break;
      case "cost": getCost(args[1]);
        break;
      case "setCost": setCost(args[1], Integer.parseInt(args[2]));
        break;
      default: usage();
        break;
    }
  }
}
