## Objective
To get started using PostgreSQL in Docker. 

## Create a Docker ID account
Go [here](https://docs.docker.com/docker-id/) for instructions to create a _free_ Docker ID.
Follow the instructions to setup your Docker ID.

## Lab
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot. 
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. Feel free to play around.  
When you are ready, run the command below to startup a Postgres server and connect to it using psql.

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh 0`

You should see a bunch of output that ends in something like this:

```
##########################################
#     Connecting using psql client       #
##########################################
# Use Ctrl-D to exit
   test
-----------
 it works!
(1 row)

psql (10.0)
Type "help" for help.
```

You are looking specifically for the **it works!** message.  
Use **Ctrl-D** to exit out of psql.
Click on the **Delete** button to destroy the instance.

## psql Reference

Some interesting flags (to see all, use `-h`):
- `-E`: will describe the underlaying queries of the `\` commands (cool for learning!)
- `-l`: psql will list all databases and then exit (useful if the user you connect with doesn't has a default database, like at AWS RDS)

Most `\d` commands support additional param of `__schema__.name__` and accept wildcards like `*.*`

- `\q`: Quit/Exit
- `\c __database__`: Connect to a database
- `\d __table__`: Show table definition including triggers
- `\dt *.*`: List tables from all schemas (if `*.*` is omitted will only show SEARCH_PATH ones)
- `\l`: List databases
- `\dn`: List schemas
- `\df`: List functions
- `\dv`: List views
- `\df+ __function__` : Show function SQL code. 
- `\x`: Pretty-format query results instead of the not-so-useful ASCII tables

