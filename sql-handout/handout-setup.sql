drop table Students cascade;
create table Students(
  PID char(40) NOT NULL, 
  Name char(40) NOT NULL, 
  Address char(40) NOT NULL,
  primary key (pid));

drop table Professors cascade;
create table Professors(
  PID char(40) NOT NULL, 
  Name char(40) NOT NULL, 
  Office char(40) NOT NULL, 
  Age integer NOT NULL, 
  DepartmentName char(40) NOT NULL, 
  Salary integer NOT NULL,
  primary key (pid));

drop table Courses cascade;
create table Courses(
  Number integer NOT NULL, 
  DeptName char(40) NOT NULL, 
  CourseName char(40) NOT NULL, 
  Classroom char(40) NOT NULL, 
  Enrollment integer NOT NULL,
  primary key (DeptName, Number));

drop table Departments cascade;
create table Departments(
  Name char(40) NOT NULL, 
  ChairPID char(40),
  primary key (Name),
  unique (ChairPID));
alter table professors add foreign key (DepartmentName) references Departments;
alter table Courses add foreign key (DeptName) references Departments;

drop table Take cascade;
create table Take(
  StudentPID char(40) NOT NULL, 
  Number integer NOT NULL, 
  DeptName char(40) NOT NULL, 
  Grade char(40), 
  ProfessorEvaluation integer,
  primary key (StudentPID, Number, DeptName),
  foreign key (StudentPid) references Students,
  foreign key (DeptName, Number) references Courses);

drop table Teach cascade;
create table Teach(
  ProfessorPID char(40) NOT NULL, 
  Number integer NOT NULL, 
  DeptName char(40) NOT NULL,
  primary key (ProfessorPID, DeptName, Number),
  foreign key (ProfessorPID) references Professors,
  foreign key (DeptName, Number) references Courses
  );

drop table PreReq cascade;
create table PreReq(
  Number integer NOT NULL, 
  DeptName char(40) NOT NULL, 
  PreReqNumber integer NOT NULL, 
  PreReqDeptName char(40) NOT NULL,
  primary key (DeptName, Number, PreReqDeptName, PreReqNumber),
  foreign key (DeptName, Number) references Courses,
  foreign key (PreReqDeptName, PreReqNumber) references Courses
  );

