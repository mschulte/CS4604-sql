## Objective
Learn how to use [WITH RECURSIVE](https://www.postgresql.org/docs/current/static/queries-with.html) in Postgresql.

## Lab

### Setup
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh demo-recursion`

You should see a bunch of output that ends in something like this:

```
TODO
```

### WITH RECURSIVE

Postgresql offers a WITH RECURSIVE clause that allows us to accomplish things not otherwise possible in standard SQL. 
Using RECURSIVE, a WITH query can refer to its own output.

**Note:** This is not standard SQL. It is specific to Postgresql.

#### Setup Test Data

The database has been prepoulated with a `tree` relation using the SQL below.

```
DROP table if exists tree;

CREATE TABLE tree (
 id SERIAL PRIMARY KEY,
 name character varying(64) NOT NULL,
 parent_id integer REFERENCES tree NULL
);

insert into tree values (1, 'Grandfather', NULL);
insert into tree values (2, 'Father', 1);
insert into tree values (3, 'Son', 2);
insert into tree values (4, 'Grandson', 3);
```

If you want to re-initialize the database, simply copy and paste the above into your psql session.

#### The Data

To vew the data, execute `select * from tree;`. You should see something like:

```
 id |        name         | parent_id 
----+---------------------+-----------
  1 | Grandfather         |          
  2 | Father              |         1
  3 | Son                 |         2
  4 | Grandson            |         3
```

As you can see the parent_id column points to an id in a different row.

#### The Query

If you wanted to find all of the descendents of 'Father', you can do so by executing a **WITH RECURSIVE** query:

```
WITH RECURSIVE 
    -- descendants 
    rec_d (id, name) AS
    (
      SELECT tree.id, tree.name FROM tree WHERE name = 'Father'
      UNION ALL
      SELECT tree.id, tree.name FROM rec_d, tree where tree.parent_id = rec_d.id
    )
SELECT id, name FROM rec_d 
 WHERE name != 'Father'
;
```

(You can include the Father in the result set by removing the `WHERE name != 'Father'` clause at the end)

Similarly, if I wanted to find all descdendents of 'Son', I would adjust my query:

```
WITH RECURSIVE
    -- descendants
    rec_d (id, name) AS
    (
      SELECT tree.id, tree.name FROM tree WHERE name = 'Son'
      UNION ALL
      SELECT tree.id, tree.name FROM rec_d, tree where tree.parent_id = rec_d.id
    )
SELECT id, name FROM rec_d
 WHERE name != 'Son'
;
```

#### More Data

Let's add some more data and take a look at our table. Execute the following:

```
insert into tree values (6, 'Daughter', 2);
insert into tree values (7, 'Granddaughter', 3);
insert into tree values (8, 'Great-Granddaughter', 4);
insert into tree values (9, 'Great-Great-Granddaughter', 5);
select * from tree;
```

You should see something like:

```
 id |           name            | parent_id 
----+---------------------------+-----------
  1 | Grandfather               |          
  2 | Father                    |         1
  3 | Son                       |         2
  4 | Grandson                  |         3
  5 | Great-Grandson            |         4
  6 | Daughter                  |         2
  7 | Granddaughter             |         3
  8 | Great-Granddaughter       |         4
  9 | Great-Great-Granddaughter |         5
(9 rows)
```

Now let's re-execute our query looking for descendents of 'Father':

```
WITH RECURSIVE
    -- descendants
    rec_d (id, name) AS
    (
      SELECT tree.id, tree.name FROM tree WHERE name = 'Father'
      UNION ALL
      SELECT tree.id, tree.name FROM rec_d, tree where tree.parent_id = rec_d.id
    )
SELECT id, name FROM rec_d
 WHERE name != 'Father'
;
```

You should see something like:

```
 id |           name            
----+---------------------------
  3 | Son
  6 | Daughter
  4 | Grandson
  7 | Granddaughter
  5 | Great-Grandson
  8 | Great-Granddaughter
  9 | Great-Great-Granddaughter
(7 rows)
```

As you can see this is a very powerful extension to SQL that will allow us to recursively 
select from parent-child relationships.

To read more about it and other `WITH` functionality, click [here](https://www.postgresql.org/docs/current/static/queries-with.html).
```
