## Permissions
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh hw.5-security.1`

You should see a bunch of output that ends in something like this:

```
CREATE TABLE
create user regi;
CREATE ROLE
create user hrusr;
CREATE ROLE
psql (10.0)
Type "help" for help.

postgres=# 
```

From here you can practice your queries. See the homework specification for instructions.
