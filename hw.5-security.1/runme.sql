CREATE TABLE employees(
  id int primary key,
  name text not null,
  ssn char(9) not null unique,
  address text not null
);

create table departments(
  id int primary key,
  name text not null
);

create table professors(
  employee_id int primary key,
  department_id int not null,
  foreign key (employee_id) references employees,
  foreign key (department_id) references departments
); 

create table students(
  id int primary key,
  name text not null,
  ssn  char(9) not null unique,
  GPA real not null
);

create table discipline(
  student_id int not null,
  event text,
  primary key(student_id, event),
  foreign key (student_id) references students
);

create table courses(
  department_id int not null,
  course_num int not null,
  name text not null,
  primary key (department_id, course_num),
  foreign key (department_id) references departments
);

create table enrolled(
  student_id int not null,
  department_id int not null,
  course_num int not null,
  grade int,
  primary key (student_id, department_id, course_num),
  foreign key (student_id) references students,
  foreign key (department_id, course_num) references courses
);

create table teaches(
  Professor_Id int not null,
  Department_Id int not null,
  Course_Num int not null,
  primary key (professor_id, department_id, course_num),
  foreign key (professor_id) references professors,
  foreign key (department_id, course_num) references courses
);

create user regi;
create user hrusr;
create user gradstud;
