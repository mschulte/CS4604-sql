# CS4604 Labs

This course will primarily use [PostgreSQL](https://www.postgresql.org/) running in [Docker](http://www.docker.com).

Docker provides a consistent environment in which to run the database. You are encouraged, but not required to
learn more about it. This course will use the [Docker Playground](http://play-with-docker.com) for labs.
Docker Playground sessions are limited to four hours. This should be more than sufficient for the lab work you 
will be doing in this course. 

PostgreSQL (pronounced "post-gress-Q-L") is an open source relational database management system (DBMS). 
PostgreSQL DBMS and its source code is available free of charge.

## Labs

Each individual folder has instructions for that particular lab. For example, see [0.test-setup](0.test-setup)

#### Attribution

The following have _greatly_ contributed to the content in these labs:

* [Dr. B. Aditya Prakash](http://people.cs.vt.edu/~badityap/)
* [W3 Schools](https://www.w3schools.com/sql/) 
* [WebGoat](https://github.com/WebGoat/WebGoat)
* [Kaggle](https://www.kaggle.com/)
