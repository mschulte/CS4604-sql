CREATE TABLE warmup(
  Complaint_ID TEXT PRIMARY KEY,
  Date_received TEXT,
  Product TEXT,
  Issue TEXT,
  Consumer_complaint_narrative TEXT,
  Company_public_response TEXT,
  Company TEXT,
  State TEXT,
  ZIP_code TEXT,
  Date_sent_to_company TEXT,
  Company_response_to_consumer TEXT,
  Timely_response TEXT,
  Consumer_disputed TEXT
);

\COPY warmup FROM '/sql/complaints-warm-up.csv' WITH (FORMAT csv);

CREATE TABLE complaints(
  Complaint_ID TEXT PRIMARY KEY,
  Date_received TEXT,
  Product TEXT,
  Issue TEXT,
  Consumer_complaint_narrative TEXT,
  Company_public_response TEXT,
  Company TEXT,
  State TEXT,
  ZIP_code TEXT,
  Date_sent_to_company TEXT,
  Company_response_to_consumer TEXT,
  Timely_response TEXT,
  Consumer_disputed TEXT
);

\COPY complaints FROM '/sql/complaints.csv' WITH (FORMAT csv);


