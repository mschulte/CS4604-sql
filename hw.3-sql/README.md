## Objective
Query a database table

## Lab
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh hw.3-sql`

You should see a bunch of output that ends in something like this:

```
##########################################
#     Connecting using psql client       #
##########################################
# Use Ctrl-D to exit
CREATE TABLE warmup(
  "Complaint ID" TEXT,
  "Date received" TEXT,
  "Product" TEXT,
  "Issue" TEXT,
  "Consumer complaint narrative" TEXT,
  "Company public response" TEXT,
  "Company" TEXT,
  "State" TEXT,
  "ZIP code" TEXT,
  "Date sent to company" TEXT,
  "Company response to consumer" TEXT,
  "Timely response?" TEXT,
  "Consumer disputed?" TEXT
);
CREATE TABLE
\COPY warmup FROM '/sql/complaints-warm-up.csv' WITH (FORMAT csv);
COPY 50
CREATE TABLE complaints(
  "Complaint ID" TEXT,
  "Date received" TEXT,
  "Product" TEXT,
  "Issue" TEXT,
  "Consumer complaint narrative" TEXT,
  "Company public response" TEXT,
  "Company" TEXT,
  "State" TEXT,
  "ZIP code" TEXT,
  "Date sent to company" TEXT,
  "Company response to consumer" TEXT,
  "Timely response?" TEXT,
  "Consumer disputed?" TEXT
);
CREATE TABLE
\COPY complaints FROM '/sql/complaints.csv' WITH (FORMAT csv);
COPY 2016
psql (10.0)
Type "help" for help.

postgres=# 
```

From here you can enter queries against the `warmup` and `complaints` tables. See the homework specification for instructions.
